package com.momodding.testing2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Testing2Application

fun main(args: Array<String>) {
    runApplication<Testing2Application>(*args)
}
