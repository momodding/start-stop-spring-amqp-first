package com.momodding.testing2.service

import com.momodding.testing2.service.MessageService
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QueueListener @Autowired constructor(
    private val messageService: MessageService
) : MessageListener {

    override fun onMessage(message: Message) {
        messageService.messageListener(message)
    }
}