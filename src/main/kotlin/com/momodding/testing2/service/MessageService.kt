package com.momodding.testing2.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MessageService @Autowired constructor() {

    fun messageListener(message: Message) {
        println(jacksonObjectMapper().writeValueAsString(message.body))
    }
}