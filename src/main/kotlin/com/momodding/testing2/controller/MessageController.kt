package com.momodding.testing2.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(value = ["/message"], produces = [MediaType.APPLICATION_JSON_VALUE])
class MessageController @Autowired constructor(
    @Qualifier("simpleListenerContainer")
    private val rabbitListener: SimpleMessageListenerContainer
) {

    @GetMapping("stop-listen")
    fun stopListen(): ResponseEntity<Any> = try {
        rabbitListener.stop()
        ResponseEntity(true, HttpStatus.OK)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ResponseEntity(false, HttpStatus.BAD_REQUEST)
    }

    @GetMapping("start-listen")
    fun startListen(): ResponseEntity<Any> = try {
        rabbitListener.start()
        ResponseEntity(true, HttpStatus.OK)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ResponseEntity(false, HttpStatus.BAD_REQUEST)
    }
}