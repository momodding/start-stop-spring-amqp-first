package com.momodding.testing2.config

import com.momodding.testing2.service.QueueListener
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ListenerConfig @Autowired constructor(
    private val queueListener: QueueListener
) {

    @Bean
    fun simpleListenerContainer(connectionFactory: ConnectionFactory): SimpleMessageListenerContainer {
        return SimpleMessageListenerContainer().apply {
            setConnectionFactory(connectionFactory)
            setQueueNames("nyoba_message")
            setMessageListener(queueListener)
        }
    }
}